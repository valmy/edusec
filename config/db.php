<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=mysql;dbname=edusec',
    'username' => 'root',
    'password' => 'edusec',
    'charset' => 'utf8',
    'enableSchemaCache' => true,
];
